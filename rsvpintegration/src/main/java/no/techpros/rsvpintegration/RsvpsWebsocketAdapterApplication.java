package no.techpros.rsvpintegration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;

@SpringBootApplication
public class RsvpsWebsocketAdapterApplication {
    @Value("${wespocketuri}")
    private String MEETUP_RSVPS_ENDPOINT;

    public static void main(String[] args) {
        SpringApplication.run(RsvpsWebsocketAdapterApplication.class, args);
    }

    @Bean
    public ApplicationRunner initiallizeConnection(RsvpsWebSocketHandler rsvpsWebSocketHandler){

        return args ->{
                WebSocketClient rsvpsSocketClient = new StandardWebSocketClient();
                rsvpsSocketClient.doHandshake(rsvpsWebSocketHandler,MEETUP_RSVPS_ENDPOINT);
            };
    }
}
