package no.techpros.templateresourceserver.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableWebSecurity
public class BasicSecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Value("${jwt.secret}")
    private String jwtSecret;
    @Value("${jwt.issuer}")
    private String jwtIssuer;
    @Value("${jwt.type}")
    private String jwtType;
    @Value("${jwt.audience}")
    private String jwtAudience;

    /**
     * The encryption SALT.
     */
    private static final String SALT = "12323*&^%of";

//    @Bean
//    public BCryptPasswordEncoder passwordEncoder() {
//        return new BCryptPasswordEncoder(); //12, new SecureRandom(SALT.getBytes()));
//    }

//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth
//                .inMemoryAuthentication()
//                .withUser("emma").password("{noop}emma").roles("MEMBER", "BOARD").and()
//                .withUser("chris").password("{bcrypt}chris").roles("BOARD");
//    }

    /**
     * Roles
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //Authorization: Basic QWxhZGRpbjpPcGVuU2VzYW1l
        http
        .cors().and().csrf().disable()
                .addFilter(new JwtAuthenticationFilter(authenticationManager(), jwtAudience, jwtIssuer, jwtSecret, jwtType))
                .authorizeRequests(authorizeRequests ->
                        authorizeRequests
                                .antMatchers("/board/*").hasAnyRole("MEMBER", "BOARD")
                                .antMatchers("/members/*").hasRole("BOARD")
                                .antMatchers("/").permitAll()
                )
//                .httpBasic().realmName("My org ream")
//                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);


//       //default
//        http.formLogin();
//
//        http.authorizeRequests()
//                .antMatchers("/devs/*").hasAnyRole("boss", "dev")
//                .antMatchers("/boss/*").hasRole("boss")
//                .antMatchers("/").permitAll();
    }

//    @Override
//    public void configure(WebSecurity web) throws Exception {
//        super.configure(web);
//    }
}
