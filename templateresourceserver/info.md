
Documentation

https://grobmeier.solutions/spring-security-5-intro-form-login.html


```
@EnableWebSecurity 

java.lang.illegalargumentexception there is no passwordencoder mapped for the id null spring boot
```

* https://www.dariawan.com/tutorials/spring/illegalargumentexception-there-no-passwordencoder-mapped-id-null/
* https://www.yawintutor.com/encode-decode-using-bcryptpasswordencoder-in-spring-boot-security/

Prior to Spring Security 5.0 the default PasswordEncoder was NoOpPasswordEncoder which required plain text passwords.
Spring Security 5.0 defaulted to DelegatingPasswordEncoder



* How to generate a HS512 secret key to use with JWT

(https://stackoverflow.com/questions/33960565/how-to-generate-a-hs512-secret-key-to-use-with-jwt/56934114#56934114)
```
openssl rand -base64 172 | tr -d '\n'

This you get:
1IGahtzZGk12cH0jToGDreEmFek2DJZV219s9FNBBnnXrDB2l2Ri0+IgZcPNCl66nr2u1JJn3gBEs3lBX01Fhz4uboHkWeO1/wKWZXxJHSIvL3cv1V+RwKwVWEdHb47cfbKEjNtoV3xoVHFdpWhl4wjkYJK2Osgst2piAxZgBXruSGmcJx/MpBxr8/e1X4H6h1yD31Y+8drDrb26aupTfaSc1/y7ESVbLmZwcw==%  
```



